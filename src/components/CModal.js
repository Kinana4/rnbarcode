import {
  Text,
  View,
  Modal,
  Pressable,
  StatusBar,
  StyleSheet,
  TouchableOpacity,
} from 'react-native';
import React from 'react';
import {
  moderateScale,
  verticalScale,
  horizontalScale,
} from '../helpers/sizeHelpers';

const CModal = ({ onOutsidePress, showModal, title, subText, button}) => {
  return (
    <>
      <StatusBar backgroundColor={'black'} />
      <Modal
        animationType="fade"
        transparent={true}
        visible={showModal}
        onRequestClose={onOutsidePress}>
        <TouchableOpacity
          style={styles.centeredView}
          activeOpacity={1}
          onPress={onOutsidePress}>
          <View style={styles.modalView}>
            <Text style={styles.signOutTxt}>{title}</Text>
            <Text style={styles.modalText}>{subText}</Text>
            <Pressable style={styles.button} onPress={onOutsidePress}>
              <Text style={styles.textStyle}>{button? button :'OK'}</Text>
            </Pressable>
          </View>
        </TouchableOpacity>
      </Modal>
    </>
  );
};

export default CModal;

const styles = StyleSheet.create({
  centeredView: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.6)',
  },
  modalView: {
    backgroundColor: 'white',
    borderRadius: moderateScale(2),
    paddingTop: verticalScale(15),
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: moderateScale(4),
    elevation: 5,
    width: '88%',
    paddingTop: moderateScale(10),
    paddingLeft: moderateScale(20),
  },
  button: {
    width: horizontalScale(120),
    alignSelf: 'flex-end',
    marginTop: verticalScale(10),
  },
  textStyle: {
    color: 'black',
    textAlign: 'center',
    fontWeight: '500',
    fontSize: moderateScale(14),
    padding: 20,
  },
  modalText: {
    marginTop: 10,
    fontSize: moderateScale(16),
    color: 'black',
  },
  signOutTxt: {
    marginTop: 5,
    fontSize: moderateScale(18),
    color: 'black',
    fontWeight: '500',
  },
});

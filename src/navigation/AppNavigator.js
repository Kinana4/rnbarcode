import {createStackNavigator} from '@react-navigation/stack';
import HomeScreen from '../screens/HomeScreen';
import BarcodeGeneratorScreen from '../screens/BarcodeGeneratorScreen';
import BarcodeScannerScreen from '../screens/BarcodeScannerScreen';

const Stack = createStackNavigator();

const AppNavigator = () => {
  return (
    <Stack.Navigator
      initialRouteName="HomeScreen"
      screenOptions={{headerShown: false}}>
      <Stack.Screen
        name="HomeScreen"
        component={HomeScreen}
        options={{title: 'React Native Barcode'}}
      />
      <Stack.Screen
        name="BarcodeGeneratorScreen"
        component={BarcodeGeneratorScreen}
      />
      <Stack.Screen
        name="BarcodeScannerScreen"
        component={BarcodeScannerScreen}
      />
    </Stack.Navigator>
  );
};

export default AppNavigator;

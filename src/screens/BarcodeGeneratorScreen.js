import React, {useState, useRef, useCallback} from 'react';
import {
  View,
  Platform,
  PermissionsAndroid,
  Text,
  Dimensions,
  TextInput,
  SafeAreaView,
} from 'react-native';
import styles from '../styles/Style.js';
import Barcode from '@kichiyaki/react-native-barcode-generator';
import Share from 'react-native-share';
import RNFetchBlob from 'rn-fetch-blob';
import ViewShot, {captureRef} from 'react-native-view-shot';
import CHeader from '../components/CHeader.js';
import {
  horizontalScale,
  moderateScale,
  verticalScale,
} from '../helpers/sizeHelpers.js';
import CButton from '../components/CButton.js';
import CModal from '../components/CModal.js';
import Ionicon from 'react-native-vector-icons/Ionicons.js';

const BarcodeGeneratorScreen = ({navigation}) => {
  const [BarValue, setBarValue] = useState('lintangwisesa');
  const [BarImage, setBarImage] = useState('');
  const [showDialog, setShowDialog] = useState(false);
  const [loading, setloading] = useState(false);
  const ref = useRef();

  const shareQR = useCallback(() => {
    captureRef(ref, {
      format: 'jpg',
      quality: 0.8,
      result: 'base64',
    }).then(
      b64 => {
        const shareImageBase64 = {
          title: 'Barcode',
          message: 'Here is my barcode!',
          url: `data:image/jpeg;base64,${b64}`,
        };
        setBarImage(String(shareImageBase64.url));
        Share.open(shareImageBase64);
      },
      error => console.error('Oops, snapshot failed', error),
    );
  }, []);

  const downloadQR = useCallback(() => {
    setShowDialog(true);
    setloading(true);
    captureRef(ref, {
      format: 'jpg',
      quality: 0.8,
      result: 'base64',
    }).then(
      async b64 => {
        const shareImageBase64 = {
          title: 'Barcode',
          message: 'Here is my barcode!',
          url: `data:image/jpeg;base64,${b64}`,
        };
        setBarImage(String(shareImageBase64.url));

        if (Platform.OS === 'ios') {
          saveImage(String(shareImageBase64.url));
        } else {
          try {
            const granted = await PermissionsAndroid.request(
              PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
              {
                title: 'Storage Permission Required',
                message:
                  'App needs access to your storage to download the QR code image',
              },
            );
            if (granted === PermissionsAndroid.RESULTS.GRANTED) {
              console.log('Storage Permission Granted');
              saveImage(String(shareImageBase64.url));
            } else {
              console.log('Storage Permission Not Granted');
            }
          } catch (err) {
            console.log(err);
          }
        }
      },
      error => console.error('Oops, snapshot failed', error),
    );
  }, []);

  const saveImage = barcode => {
    setloading(false);
    barcode = barcode.split('data:image/jpeg;base64,')[1];

    let date = new Date();
    const {fs} = RNFetchBlob;
    let filename =
      '/barcode_' +
      Math.floor(date.getTime() + date.getSeconds() / 2) +
      '.jpeg';
    let PictureDir = fs.dirs.DownloadDir + filename;

    fs.writeFile(PictureDir, barcode, 'base64')
      .then(() => {
        RNFetchBlob.android.addCompleteDownload({
          title: '🎁 Here is your barcode!',
          useDownloadManager: true,
          showNotification: true,
          notification: true,
          path: PictureDir,
          mime: 'image/jpeg',
          description: 'Image',
        });
      })
      .catch(err => {
        console.log('ERR: ', err);
      });
  };

  return (
    <SafeAreaView style={{flex:1}}>
      <CHeader title="Generate your barcode" navigation={navigation} />
      <View style={styles.container}>
        <View
          style={{
            flexDirection: 'row',
            alignItems: 'center',
            borderBottomWidth: 0.5,
            width: '90%',
            marginBottom: verticalScale(15),
          }}>
          <Ionicon
            name={'barcode'}
            size={moderateScale(24)}
            color={'#007bff'}
          />
          <TextInput
            placeholder="Type your text here..."
            onChangeText={val => {
              setBarValue(val);
            }}
          />
        </View>
        <ViewShot ref={ref}>
          <Barcode
            format="CODE128"
            value={BarValue ? BarValue : 'lintangwisesa'}
            text={BarValue ? BarValue : 'lintangwisesa'}
            style={{marginBottom: 20}}
            textStyle={{color: '#000'}}
            maxWidth={Dimensions.get('window').width / 1.5}
          />
        </ViewShot>
        <CButton
          title="Share Barcode"
          iconName="share"
          iconLibrary="FontAwesome"
          onPress={shareQR}
          extraStyles={{
            height: verticalScale(50),
            borderRadius: moderateScale(25),
            width: horizontalScale(200),
          }}
          extraFontStyles={{fontSize: moderateScale(17)}}
          iconSize={moderateScale(20)}
        />
        <CButton
          title="Download"
          iconName="file-download"
          iconLibrary="MaterialCommunityIcons"
          onPress={downloadQR}
          extraStyles={{
            height: verticalScale(50),
            borderRadius: moderateScale(25),
            width: horizontalScale(200),
            marginTop: verticalScale(10),
          }}
          extraFontStyles={{fontSize: moderateScale(17)}}
          iconSize={moderateScale(20)}
        />
        <CModal
          onOutsidePress={() => setShowDialog(!showDialog)}
          showModal={showDialog}
          title="Download QR"
          subText="Your barcode has been downloaded successfully. Check it on your Downloads folder"
        />
      </View>
      </SafeAreaView>
  );
};

export default BarcodeGeneratorScreen;

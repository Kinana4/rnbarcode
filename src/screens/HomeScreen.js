import * as React from 'react';
import {SafeAreaView} from 'react-native';
import styles from '../styles/Style.js';
import CButton from '../components/CButton.js';
import {moderateScale, verticalScale} from '../helpers/sizeHelpers.js';

const HomeScreen = ({navigation}) => {
  return (
    <SafeAreaView style={styles.container}>
      <CButton
        title="Generate"
        iconName="barcode"
        iconLibrary="FontAwesome6"
        onPress={() => navigation.navigate('BarcodeGeneratorScreen')}
        extraStyles={{marginBottom: verticalScale(20)}}
        iconSize={moderateScale(30)}
      />
      <CButton
        title="Scanner"
        iconName="barcode-scan"
        iconLibrary="MaterialCommunityIcons"
        onPress={() => navigation.navigate('BarcodeScannerScreen')}
        iconSize={moderateScale(30)}
      />
    </SafeAreaView>
  );
};

export default HomeScreen;

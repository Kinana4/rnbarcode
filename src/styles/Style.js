import {StyleSheet} from 'react-native';
import {
  horizontalScale,
  moderateScale,
  verticalScale,
} from '../helpers/sizeHelpers';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
  iconButtonHomeContainer: {marginRight: horizontalScale(10)},
  iconButtonHome: {
    type: 'material-community',
    size: moderateScale(50),
    color: 'white',
  },
  titleButtonHome: {
    fontWeight: '700',
    fontSize: moderateScale(25),
  },
  buttonHome: {
    backgroundColor: '#0C8E4E',
    borderColor: 'transparent',
    borderWidth: 0,
    borderRadius: moderateScale(30),
    height: verticalScale(100),
  },
  buttonHomeContainer: {
    width: horizontalScale(200),
    marginHorizontal: horizontalScale(50),
    marginVertical: verticalScale(20),
  },
});

export default styles;
